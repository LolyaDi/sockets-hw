﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace PostalCode_StreetsApp
{
    public class Program
    {
        private const int QUEUE = 100;

        private const int BUFFER_SIZE = 1024 * 4;
        private static byte[] _buffer = new byte[BUFFER_SIZE];

        private static readonly string _ipAddress = "0.0.0.0";
        private static readonly int _port = 10001;

        private static Socket _serverSocket;

        private static void Start(object state)
        {
            try
            {
                _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _serverSocket.Bind(new IPEndPoint(IPAddress.Parse(_ipAddress), _port));
                _serverSocket.Listen(QUEUE);
                _serverSocket.BeginAccept(OnSocketAccepted, null);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private static void OnSocketAccepted(IAsyncResult result)
        {
            try
            {
                var clientSocket = _serverSocket.EndAccept(result);
                clientSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, OnDataReceived, clientSocket);

                _serverSocket.BeginAccept(OnSocketAccepted, null);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private static void OnDataReceived(IAsyncResult result)
        {
            try
            {
                var clientSocket = (Socket)result.AsyncState;

                int receivedSize = clientSocket.EndReceive(result);
                if (receivedSize > 0)
                {
                    var json = JObject.Parse("{\"streets\":[" +
                        "{\"postcode\":\"Z00T0B8\", \"name\":\"R. Koshkarbaev\"}," +
                        "{\"postcode\":\"Z00Y0B8\", \"name\":\"Y. Brusilovsky\"}" +
                        "]}");

                    var streets = new List<Street>();

                    foreach (var street in json["streets"])
                    {
                        streets.Add(new Street
                        {
                            PostCode = street["postcode"].ToString(),
                            Name = street["name"].ToString()
                        });
                    }

                    var postCode = Encoding.UTF8.GetString(_buffer, 0, receivedSize);
                    var selectedStreet = streets.Find(street => street.PostCode == postCode);
                    
                    clientSocket.Send(Encoding.ASCII.GetBytes((selectedStreet == null) ? "None": selectedStreet.Name));
                }

                clientSocket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, OnDataReceived, clientSocket);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }

        private static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem(Start, null);
            Console.ReadLine();
        }
    }
}
